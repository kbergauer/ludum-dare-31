﻿using UnityEngine;
using System.Collections;

public class Building : Tile 
{
	[Header("Building")]
	public AudioClip PlaceSound;

	virtual protected void Start()
	{
		if(GameManager.PlaySound && PlaceSound != null)
			AudioSource.PlayClipAtPoint(PlaceSound, transform.position);
	}
}