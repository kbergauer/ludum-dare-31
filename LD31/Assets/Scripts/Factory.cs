﻿using UnityEngine;
using System.Collections;

public class Factory : Building
{
	public delegate void FactoryHandler();
	public event FactoryHandler OnAddWorker;
	public event FactoryHandler OnRemoveWorker;

	[Header("Production parameters")]
	public WorkerSpawn[] Workers = null;
	public Vector3 ProductionOffset = Vector3.zero;
	public float ProductionIntervall = 1.5f;
	public int Capacity = 3;

	private GameManager manager = null;
	private float timer = 0.0f;
	private int currentCapacity = 0;
	private Worker[] workers;
	private float maximumChance = 0.0f;

	public int CurrentCapacity
	{
		get { return currentCapacity; }
	}

	protected override void Start()
	{
		workers = new Worker[Capacity];

		GameObject go = GameObject.FindGameObjectWithTag("GameManager");
		if(go != null)
			manager = go.GetComponent<GameManager>();

		for(int i = 0; i < Workers.Length; i++)
			if(Workers[i] != null && Workers[i].Prefab != null)
				maximumChance += Workers[i].Chance;

		base.Start();
	}

	void Update()
	{
		if(IsFull())
			return;

		timer += Time.deltaTime;
		if(timer >= ProductionIntervall)
		{
			CreateWorker();
			timer = 0.0f;
		}
	}

	public bool IsFull()
	{
		return currentCapacity == Capacity;
	}

	private void CreateWorker()
	{
		if(IsFull() || !manager.CanPopulate())
			return;

		// Determine prefab
		int chosen = -1;
		float chance = 0.0f;
		for(int i = 0; i < Workers.Length; i++)
		{
			if(Workers[i] == null || Workers[i].Prefab == null)
				continue;

			chance += Workers[i].Chance;
			if(Random.Range(0.0f, maximumChance) <= chance)
			{
				chosen = i;
				break;
			}
		}

		// Instantiate prefab
		if(chosen >= 0)
		{
			Worker worker = Instantiate(Workers[chosen].Prefab, transform.position + ProductionOffset, Quaternion.identity) as Worker;
			AddWorker(worker);
		}
	}

	private void AddWorker(Worker _Worker)
	{
		workers[currentCapacity] = _Worker;
		workers[currentCapacity].SetManufacturer(this);

		if(OnAddWorker != null)
			OnAddWorker();

		currentCapacity++;
	}

	public void RemoveWorker(Worker _Worker)
	{
		for(int i = 0; i < currentCapacity; i++)
			if(workers[i] == _Worker)
				workers[i] = null;

		if(OnRemoveWorker != null)
			OnRemoveWorker();

		currentCapacity--;
	}
}