﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	public static bool PlaySound = true;
	public static bool PlayMusic = true;

	[Header("Input")]
	public RectTransform MenuPopup = null;
	public SpriteRenderer SelectionPreview = null;
	public Color SelectionColor = Color.blue;
	public Color OccupationColor = Color.red;
	public Text CostPopup = null;

	[Header("Statistics")]
	public int Currency = 0;
	public float FactoryCost = 2000;
	public float FactoryCostIncrease = 2.0f;
	public float ProductionCost = 500;
	public float ProductionCostIncrease = 1.5f;
	public float HousingCost = 500;
	public float HousingCostIncrease = 1.5f;

	[Header("Interface")]
	public Text WitherUI = null;
	public Text CurrencyUI = null;
	//public Text SelectionUI = null;
	public Text PopulationUI = null;
	public Text PopulationCapUI = null;
	public Text CostUIFactory = null;
	//public Image CostUIFactoryIcon = null;
	public Text CostUIProduction = null;
	//public Image CostUIProductionIcon = null;
	public Text CostUIHousing = null;
	//public Image CostUIHousingIcon = null;
	public Text CountUIFactory = null;
	public Text CountUIProduction = null;
	public Text CountUIHousing = null;
	public Color ColorRed = Color.red;
	public Color ColorOrange = Color.yellow;
	public Color ColorGreen = Color.green;
	public float ColorFadingSpeed = 1.0f;
	//public Image UICorner = null;
	//public Image DigitPrefab = null;
	//public Sprite[] Numbers = null;

	[Header("Game Over")]
	public float TimeSurvived = 0.0f;
	public GameOver GameOverUI = null;

	// General
	private Map map;
	private Vector2 position;
	private bool destroyed = false;
	private Coroutine witherProgressAnimation = null;

	// Work
	private WorkManager work;

	// Selecting
	private Tile hit;

	// Coloring
	//private Tile enter;
	//private Color oldColor;

	// Currency and buying
	private List<Factory> factories;
	private List<Production> producers;
	private List<Housing> houses;
	//private int oldCurrency = -1;
	//private string currencyString = "";
	//private float xPosition = 20.0f;
	//private List<Image> digits = null;
	private Tile.EType selection = Tile.EType.Factory;
	private Transform canvas = null;

	// Population
	private int populationCap = 0;
	private int populationCurrent = 0;

	void Awake()
	{
		GameObject go = GameObject.FindGameObjectWithTag("Map");
		if(go != null)
		{
			map = go.GetComponent<Map>();
			map.OnGameOver += GameOver;
		}

		go = GameObject.FindGameObjectWithTag("WorkManager");
		if(go != null)
			work = go.GetComponent<WorkManager>();

		go = GameObject.FindGameObjectWithTag("PopupContainer");
		if(go != null)
			canvas = go.transform;

		factories = new List<Factory>();
		producers = new List<Production>();
		houses = new List<Housing>();
		//digits = new List<Image>();

		// Get initial housing list
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Factory");
		Factory factory = null;
		for(int i = 0; i < objects.Length; i++)
		{
			factory = objects[i].GetComponent<Factory>();
			if(factory != null)
			{
				//Debug.Log(this.name + ": Found factory " + factory.name);
				factories.Add(factory);
				factory.OnWither += RemoveFactory;
				factory.OnAddWorker += AddPopulation;
				factory.OnRemoveWorker += SubtractPopulation;
			}
		}

		// Get initial housing list
		objects = GameObject.FindGameObjectsWithTag("Housing");
		Housing housing = null;
		for(int i = 0; i < objects.Length; i++)
		{
			housing = objects[i].GetComponent<Housing>();
			if(housing != null)
			{
				//Debug.Log(this.name + ": Found housing " + housing.name);
				houses.Add(housing);
				housing.OnWither += RemoveHousing;

				populationCap += housing.Spots;
			}
		}
	}

	void Start()
	{
		// Get initial housing list
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Production");
		Production producer = null;
		for(int i = 0; i < objects.Length; i++)
		{
			producer = objects[i].GetComponent<Production>();
			if(producer != null)
			{
				//Debug.Log(this.name + ": Found housing " + housing.name);
				producers.Add(producer);
				producer.OnWither += RemoveProducer;
			}
		}

		SetSelection(Tile.EType.Factory);
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.KeypadEnter))
			GameOver();

		// Save time survived
		TimeSurvived += Time.deltaTime;

		// Highlight current tile
		/*
		if(enter != null)
			enter.Renderer.color = oldColor;
		*/
		position = map.GetTileCoordinates(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		hit = map.GetTile(position);
		if(hit != null)
		{
			if(!SelectionPreview.gameObject.activeInHierarchy)
				SelectionPreview.gameObject.SetActive(true);

			SelectionPreview.transform.position = hit.transform.position;

			//oldColor = enter.Renderer.color;
			if(hit.Type == Tile.EType.Empty)
				//enter.Renderer.color *= SelectionColor;
				SelectionPreview.color = SelectionColor;
			else
				//enter.Renderer.color *= OccupationColor;
				SelectionPreview.color = OccupationColor;
		}
		else
			SelectionPreview.gameObject.SetActive(false);

		//Debug.Log(this.name + "mouse=" + Input.mousePosition);

		// Left mouse button
		if(Input.GetMouseButtonDown(0))
		{
			bool onPopup = false;
			if(MenuPopup.gameObject.activeInHierarchy)
			{
				Vector2 mouse = Input.mousePosition;
				Vector2 fix = new Vector2(Screen.width / 640.0f, Screen.height / 480.0f);
				Rect popup = new Rect(MenuPopup.anchoredPosition.x * fix.x, MenuPopup.anchoredPosition.y * fix.y, MenuPopup.sizeDelta.x * fix.x, MenuPopup.sizeDelta.y * fix.y);

				onPopup = mouse.x >= popup.x && mouse.x < popup.x + popup.width && mouse.y < popup.y && mouse.y >= popup.y - popup.height;
			}

			if(!onPopup)
			{
				// Get selection cost
				int cost = (int)GetCost(selection);

				// Check funds
				if(Currency >= cost)
				{
					// Get clicked tile	
					hit = map.GetTile(position);

					if(hit != null && hit.Type == Tile.EType.Empty)
					{
						// Create and add tile to map
						hit = map.CreateTile((int)position.x, (int)position.y, selection);
						map.AddTile((int)position.x, (int)position.y, hit);

						// Do tile specific initialization
						switch(selection)
						{
							case Tile.EType.Factory:
								work.AddFactory(hit as Factory);
								factories.Add(hit as Factory);
								hit.OnWither += RemoveFactory;
								factories[factories.Count - 1].OnAddWorker += AddPopulation;
								factories[factories.Count - 1].OnRemoveWorker += SubtractPopulation;
								break;

							case Tile.EType.Production:
								producers.Add(hit as Production);
								hit.OnWither += RemoveProducer;
								break;

							case Tile.EType.Housing:
								houses.Add(hit as Housing);
								hit.OnWither += RemoveHousing;
								populationCap += houses[houses.Count - 1].Spots;
								break;
						}

						// Show payment popup
						Text popup = Instantiate(CostPopup) as Text;
						popup.text = "-" + cost;
						popup.rectTransform.SetParent(canvas);
						popup.rectTransform.position = Camera.main.WorldToScreenPoint(hit.transform.position);

						// Subtract cost from funds
						Currency -= cost;

						// Animate building count
						AnimateBuildingCount(ColorGreen, selection);

						// Update preview
						SetSelectionPreviewSprite(map.GetNextTileSprite(selection));
					}
				}

				if(MenuPopup.gameObject.activeInHierarchy)
					MenuPopup.gameObject.SetActive(false);
			}
		}

		// Right mouse button
		if(Input.GetMouseButtonUp(1))
		{
			// Close menu
			if(MenuPopup.gameObject.activeInHierarchy)
			{
				MenuPopup.gameObject.SetActive(false);
			}
			// Open menu
			else
			{
				// Set menu position
				Vector3 mouse = Input.mousePosition;
				Vector2 fix = new Vector2(Screen.width / 640.0f, Screen.height / 480.0f);
				mouse.x = Mathf.Clamp(mouse.x, 0.0f, Screen.width - (MenuPopup.sizeDelta.x * fix.x));
				mouse.y = Mathf.Clamp(mouse.y, MenuPopup.sizeDelta.y * fix.y, Screen.height);
				MenuPopup.position = mouse;

				// Activate menu
				MenuPopup.gameObject.SetActive(true);
			}
		}

		// Keep UI updated
		UpdateUI();
	}

	private void AddPopulation()
	{
		populationCurrent++;
	}

	private void SubtractPopulation()
	{
		populationCurrent--;
}	

	public bool CanPopulate()
	{
		return populationCurrent < populationCap;
	}

	private void RemoveFactory(Tile _Target)
	{
		Factory factory = _Target as Factory;
		if(factory != null && factories.Contains(factory))
		{
			factories.Remove(factory);
			_Target.OnWither -= RemoveFactory;

			AnimateBuildingCount(ColorRed, Tile.EType.Factory);
		}
	}

	private void RemoveProducer(Tile _Target)
	{
		Production producer = _Target as Production;
		if(producer != null && producers.Contains(producer))
		{
			producers.Remove(producer);
			_Target.OnWither -= RemoveProducer;

			AnimateBuildingCount(ColorRed, Tile.EType.Production);
		}
	}

	private void RemoveHousing(Tile _Target)
	{
		Housing housing = _Target as Housing;
		if(housing != null && houses.Contains(housing))
		{
			houses.Remove(housing);
			_Target.OnWither -= RemoveHousing;

			populationCap -= housing.Spots;
			AnimateBuildingCount(ColorRed, Tile.EType.Housing);
		}
	}

	private void UpdateUI()
	{
		// Show wither progress
		if(WitherUI != null)
		{
			float progress = map.GetWitherProgress();
			WitherUI.text = (progress * 100.0f).ToString("00") + "%";

			// Normal display
			if(progress < 0.8f)
			{
				if(witherProgressAnimation != null)
				{
					StopCoroutine("AnimateWitherProgress");
					witherProgressAnimation = null;
				}

				WitherUI.color = Color.white;
			}
			// Animated warning display
			else if(progress < 0.9f)
			{
				if(witherProgressAnimation == null)
					witherProgressAnimation = StartCoroutine("AnimateWitherProgress", 0.5f);
			}
			// Red display
			else
			{
				if(witherProgressAnimation != null)
				{
					StopCoroutine("AnimateWitherProgress");
					witherProgressAnimation = null;
				}

				WitherUI.color = ColorRed;
			}
		}

		// Show funds
		if(CurrencyUI != null)
			CurrencyUI.text = Currency.ToString();

		// Show building counts
		if(CountUIFactory != null)
			CountUIFactory.text = factories.Count.ToString("00");

		if(CountUIProduction != null)
			CountUIProduction.text = producers.Count.ToString("00");

		if(CountUIHousing != null)
			CountUIHousing.text = houses.Count.ToString("00");

		// Show next building cost
		int cost = 0;
		if(CostUIFactory != null)
		{
			cost = (int)GetCost(Tile.EType.Factory);
			CostUIFactory.text = (cost).ToString();
			if(cost > Currency)
				CostUIFactory.color = ColorRed;
			else
				CostUIFactory.color = Color.white;
		}

		if(CostUIProduction != null)
		{
			cost = (int)GetCost(Tile.EType.Production);
			CostUIProduction.text = (cost).ToString();
			if(cost > Currency)
				CostUIProduction.color = ColorRed;
			else
				CostUIProduction.color = Color.white;
		}

		if(CostUIHousing != null)
		{
			cost = (int)GetCost(Tile.EType.Housing);
			CostUIHousing.text = (cost).ToString();
			if(cost > Currency)
				CostUIHousing.color = ColorRed;
			else
				CostUIHousing.color = Color.white;
		}

		// Show population
		if(PopulationUI != null)
		{
			PopulationUI.text = populationCurrent.ToString("00");

			if(populationCurrent >= populationCap)
				PopulationUI.color = ColorOrange;
			else
				PopulationUI.color = Color.white;
		}

		// Show population cap (housing spots)
		if(PopulationCapUI != null)
		{
			PopulationCapUI.text = populationCap.ToString("00");

			if(populationCap < factories.Count * (map.FactoryTile as Factory).Capacity)
				PopulationCapUI.color = ColorRed;
			else
				PopulationCapUI.color = Color.white;
		}

		/* OLD CODE USING SPRITES
		if(oldCurrency == Currency)
			return;

		currencyString = Currency.ToString();
		xPosition = 0.0f + UICorner.rectTransform.sizeDelta.x;

		// Get necessary digits
		if(digits.Count < currencyString.Length)
		{
			for(int i = digits.Count; i < currencyString.Length; i++)
				digits.Add(Instantiate(DigitPrefab) as Image);
		}

		// Set the digits
		bool parsed = false;
		int id = 0;
		for(int i = 0; i < currencyString.Length; i++)
		{
			// Set digit position and sprite
			digits[i].rectTransform.SetParent(UICorner.rectTransform);
			digits[i].rectTransform.anchoredPosition = new Vector2(xPosition, 0.0f);
			parsed = int.TryParse(currencyString[i] + "", out id);
			if(parsed)
				digits[i].sprite = Numbers[id];
			else
				digits[i].sprite = Numbers[0];

			// Get next digit's position
			xPosition = digits[i].rectTransform.anchoredPosition.x + (digits[i].rectTransform.sizeDelta.x - 10.0f);
		}

		oldCurrency = Currency;
		*/
	}

	private void GameOver()
	{
		StopAllCoroutines();

		map.OnGameOver -= GameOver;
		map.WitherAll();

		Destroy(work.gameObject);

		GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
		if(canvas != null)
			Destroy(canvas);

		if(GameOverUI != null)
		{
			GameOver go = Instantiate(GameOverUI) as GameOver;
			go.SetTime(TimeSurvived);
		}

		destroyed = true;
		Destroy(gameObject);
	}

	public bool SetSelection(Tile.EType _Type)
	{
		if(_Type != Tile.EType.Factory && _Type != Tile.EType.Production && _Type != Tile.EType.Housing)
			return false;

		selection = _Type;

		SetSelectionPreviewSprite(map.GetNextTileSprite(selection));

		if(MenuPopup.gameObject.activeInHierarchy)
			MenuPopup.gameObject.SetActive(false);

		return true;
	}

	public Tile.EType GetSelection()
	{
		return selection;
	}

	public void SetSelectionPreviewSprite(Sprite _Sprite)
	{
		if(SelectionPreview != null && _Sprite != null)
			SelectionPreview.sprite = _Sprite;
	}

	private float GetCost(Tile.EType _Type)
	{
		float cost = 0.0f;

		switch(_Type)
		{
			case Tile.EType.Factory:
				{
					cost = FactoryCost + FactoryCost * FactoryCostIncrease * Mathf.Max(factories.Count - map.Factories, 0);
				} break;

			case Tile.EType.Production:
				{
					cost = ProductionCost + ProductionCost * ProductionCostIncrease * Mathf.Max(producers.Count - map.Producers, 0);
				} break;

			case Tile.EType.Housing:
				{
					cost = HousingCost + HousingCost * HousingCostIncrease * Mathf.Max(houses.Count - map.Housing, 0);
				} break;
		}

		return cost;
	}

	public void ToggleSound(bool _Value)
	{
		PlaySound = _Value;
	}

	public void ToggleMusic(bool _Value)
	{
		PlayMusic = _Value;

		if(audio != null)
			audio.mute = !PlayMusic;
	}

	private void AnimateBuildingCount(Color _Color, Tile.EType _Type)
	{
		if(destroyed)
			return;

		switch(_Type)
		{ 
			case Tile.EType.Factory:
				StopCoroutine("AnimateFactoryCount");
				StartCoroutine("AnimateFactoryCount", _Color);
				break;

			case Tile.EType.Production:
				StopCoroutine("AnimateProductionCount");
				StartCoroutine("AnimateProductionCount", _Color);
				break;

			case Tile.EType.Housing:
				StopCoroutine("AnimateHousingCount");
				StartCoroutine("AnimateHousingCount", _Color);
				break;
		}
	}

	private IEnumerator AnimateFactoryCount(Color _Color)
	{
		Color color = _Color;
		CountUIFactory.color = color;

		while(color.r < 1.0f || color.g < 1.0f || color.b < 1.0f)
		{
			color.r += ColorFadingSpeed * Time.deltaTime;
			color.g += ColorFadingSpeed * Time.deltaTime;
			color.b += ColorFadingSpeed * Time.deltaTime;

			CountUIFactory.color = color;

			yield return new WaitForEndOfFrame();
		}

		CountUIFactory.color = Color.white;
	}

	private IEnumerator AnimateProductionCount(Color _Color)
	{
		Color color = _Color;
		CountUIProduction.color = color;

		while(color.r < 1.0f || color.g < 1.0f || color.b < 1.0f)
		{
			color.r += ColorFadingSpeed * Time.deltaTime;
			color.g += ColorFadingSpeed * Time.deltaTime;
			color.b += ColorFadingSpeed * Time.deltaTime;

			CountUIProduction.color = color;

			yield return new WaitForEndOfFrame();
		}

		CountUIProduction.color = Color.white;
	}

	private IEnumerator AnimateHousingCount(Color _Color)
	{
		Color color = _Color;
		CountUIHousing.color = color;

		while(color.r < 1.0f || color.g < 1.0f || color.b < 1.0f)
		{
			color.r += ColorFadingSpeed * Time.deltaTime;
			color.g += ColorFadingSpeed * Time.deltaTime;
			color.b += ColorFadingSpeed * Time.deltaTime;

			CountUIHousing.color = color;

			yield return new WaitForEndOfFrame();
		}

		CountUIHousing.color = Color.white;
	}

	private IEnumerator AnimateWitherProgress(float _Speed)
	{
		bool red = true;
		WitherUI.color = ColorRed;

		while(true)
		{
			yield return new WaitForSeconds(_Speed);

			if(red)
			{
				red = false;
				WitherUI.color = Color.white;
			}
			else
			{
				red = true;
				WitherUI.color = ColorRed;
			}
		}
	}
}
