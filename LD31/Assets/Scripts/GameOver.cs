﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver : MonoBehaviour 
{
	[Header("Elements")]
	public Text TimeText = null;

	[Header("Parameters")]
	public float TimeSurvived = 0.0f;

	public void SetTime(float _Time)
	{
		TimeSurvived = _Time;
		TimeText.text = TimeSurvived.ToString(".00");
	}

	public void Retry()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
}
