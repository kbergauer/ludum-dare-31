﻿using UnityEngine;
using System.Collections;

public class Housing : Building 
{
	[Header("Housing")]
	public int Spots = 2;
}
