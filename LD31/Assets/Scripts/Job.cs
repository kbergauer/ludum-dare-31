﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Job 
{
	public Vector2 Position = Vector2.zero;
	public Tile Broken = null;
	public Tile Replacement = null;

	public Job(int _X, int _Y, Tile _Broken)
	{
		Position = new Vector2(_X, _Y);
		Broken = _Broken;
	}

	public bool Finish(Map _Map)
	{
		if(Broken == null || Replacement == null)
			return false;

		Replacement.Renderer.sortingOrder = -10000;
		_Map.AddTile((int)Position.x, (int)Position.y, Replacement);

		return true;
	}
}
