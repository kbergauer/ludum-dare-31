﻿using UnityEngine;
using System.Collections;

public class Map : MonoBehaviour
{
	public delegate void MapHandler();
	public event MapHandler OnGameOver;

	[Header("Tiles")]
	public Tile BrokenTile = null;
	public Tile[] NormalTiles = null;
	public Tile[] OccupiedTiles = null;
	public Tile FactoryTile = null;
	public Tile[] ProductionTiles = null;
	public Tile[] HousingTiles = null;

	[Header("Parameters")]
	public int Width = 800;
	public int Height = 600;
	public int TileSize = 20;
	
	[Header("Generation")]
	public float GrassOccurence = 0.3f;
	public float BlockerOccurence = 0.1f;
	public int Factories = 1;
	public int Producers = 2;
	public int Housing = 1;

	[Header("Withering")]
	public float MaxWitherTimer = 2.0f;
	public float MinWitherTimer = 0.1f;
	public float MinTimeToNextDecrease = 0.5f;
	public float IntervallDecrease = 0.1f;
	public float GameOver = 0.9f;


	// Map variables
	private Tile[,] tiles;
	private int mapWidth;
	private int mapHeight;

	// Withering
	private Tile witherTarget;
	private Vector2 witherPosition;
	private int amountBroken = 0;
	private float witherIntervall = 0.0f;
	private float witherTimer = 0.0f;
	private float decreaseIntervall = 0.0f;
	private float decreaseTimer = 0.0f;

	// Creation
	private int nextProduction = 0;
	private int nextHousing = 0;

	void Awake()
	{
		// Get dimensions
		mapWidth = Width / TileSize;
		mapHeight = Height / TileSize;

		int x = 0, y = 0;

		// Create array and instantiate tiles
		tiles = new Tile[mapWidth, mapHeight];
		for(y = 0; y < mapHeight; y++)
		{
			for(x = 0; x < mapWidth; x++)
			{
				AddTile(x, y, CreateTile(x, y, Tile.EType.Empty));
			}
		}

		// Create housing
		x = 0;
		y = 0;
		for(int i = 0; i < Housing; i++)
		{
			x = Random.Range(0, mapWidth);
			y = Random.Range(0, mapHeight);

			AddTile(x, y, CreateTile(x, y, Tile.EType.Housing));
		}

		// Create production
		x = 0; 
		y = 0;
		for(int i = 0; i < Producers; i++)
		{ 
			x = Random.Range(0, mapWidth);
			y = Random.Range(0, mapHeight);

			AddTile(x, y, CreateTile(x, y, Tile.EType.Production));
		}

		// Create factories
		x = 0;
		y = 0;
		for(int i = 0; i < Factories; i++)
		{
			x = Random.Range(0, mapWidth);
			y = Random.Range(0, mapHeight);

			AddTile(x, y, CreateTile(x, y, Tile.EType.Factory));
		}

		// Set creation prefabs
		nextProduction = Random.Range(0, ProductionTiles.Length);
		nextHousing = Random.Range(0, HousingTiles.Length);

		// Set wither intervall
		witherIntervall = MaxWitherTimer;
		decreaseIntervall = MinTimeToNextDecrease;

		// Get wither target
		GetWitherTarget();
	}

	void Update()
	{
		// Game over aborts update
		if(amountBroken > mapWidth * mapHeight * GameOver)
			return;

		// Run timer
		witherTimer += Time.deltaTime;
		decreaseTimer += Time.deltaTime;

		// Color wither target
		if(witherTarget != null)
			witherTarget.Renderer.color = new Color(1.0f - (witherTimer / (witherIntervall * 2.0f)), 1.0f - (witherTimer / (witherIntervall * 2.0f)), 1.0f - (witherTimer / (witherIntervall * 2.0f)), 1.0f);

		// Wither target
		if(witherTimer >= witherIntervall)
		{
			// Decrease intervall
			if(witherIntervall > MinWitherTimer && decreaseTimer >= decreaseIntervall)
			{
				decreaseTimer = 0.0f;
				witherIntervall = Mathf.Max(witherIntervall - IntervallDecrease, MinWitherTimer);

				if(witherIntervall < 1.0f)
				{
					decreaseIntervall *= 1.1f;
					IntervallDecrease *= 0.9f;
				}
			}

			// Wither tile
			if(witherTarget != null)
			{
				int x = (int)witherPosition.x;
				int y = (int)witherPosition.y;

				tiles[x, y] = CreateTile(x, y, Tile.EType.Broken);
				tiles[x, y].transform.position = new Vector3(x - (mapWidth / 2.0f), y - (mapHeight / 2.0f), 0.0f);

				witherTarget.Wither(tiles[x, y]);
			}

			// Get new target
			GetWitherTarget();

			amountBroken++;
			witherTimer = 0.0f;
		}

		if(amountBroken > mapWidth * mapHeight * GameOver && OnGameOver != null)
			OnGameOver();
	}

	private void GetWitherTarget()
	{ 
		int tries = 0;
		int x = 0, y = 0;

		do
		{
			x = Random.Range(0, mapWidth);
			y = Random.Range(0, mapHeight);
			witherTarget = tiles[x, y];
			if(witherTarget != null && witherTarget.Type == Tile.EType.Broken)
				witherTarget = null;
			tries++;
		} while(witherTarget == null && tries < mapWidth * mapHeight);

		witherPosition = new Vector2(x, y);
		//witherTarget.Renderer.color = Color.gray;
	}

	public Tile CreateTile(int _X, int _Y, Tile.EType _Type)
	{
		Tile result = null;
		Tile prefab = null;

		switch(_Type)
		{
			case Tile.EType.Empty:
				{
					if(Random.Range(0.0f, 1.0f) < BlockerOccurence)
					{
						_Type = Tile.EType.Occupied;

						int variant = Random.Range(0, OccupiedTiles.Length);
						prefab = OccupiedTiles[variant];
					}
					else
					{
						int variant = 0;
						if(Random.Range(0.0f, 1.0f) < GrassOccurence)
							variant = Random.Range(1, NormalTiles.Length);

						prefab = NormalTiles[variant];
					}
				} break;

			case Tile.EType.Broken:
				{
					prefab = BrokenTile;
				} break;

			case Tile.EType.Factory:
				{
					prefab = FactoryTile;
				} break;

			case Tile.EType.Production:
				{
					prefab = ProductionTiles[nextProduction];
					nextProduction = Random.Range(0, ProductionTiles.Length);
				} break;

			case Tile.EType.Housing:
				{
					prefab = HousingTiles[nextHousing];
					nextHousing = Random.Range(0, HousingTiles.Length);
				} break;
		}

		result = Instantiate(prefab) as Tile;
		result.name = _Type.ToString() + " (" + _X + "/" + _Y + ")";
		result.transform.parent = transform;
		result.SetPosition(_X, _Y);

		return result;
	}

	public void AddTile(int _X, int _Y, Tile _Tile)
	{
		if(_X < 0 || _X >= mapWidth || _Y < 0 || _Y >= mapHeight)
			return;

		if(tiles[_X, _Y] != null && tiles[_X, _Y].Type == Tile.EType.Broken && _Tile != null && _Tile.Type != Tile.EType.Broken)
			amountBroken--;

		if(tiles[_X, _Y] != null)
			Destroy(tiles[_X, _Y].gameObject);

		tiles[_X, _Y] = _Tile;
		tiles[_X, _Y].transform.position = new Vector3(_X - (mapWidth / 2.0f), _Y - (mapHeight / 2.0f), 0.0f);
		tiles[_X, _Y].transform.parent = transform;

		if((int)witherPosition.x == _X && (int)witherPosition.y == _Y)
			witherTarget = tiles[_X, _Y];
	}

	public Vector2 GetTileCoordinates(Vector2 _Position)
	{
		Vector2 coords = _Position;

		//Debug.Log(this.name + ": Initial: " + coords.x + ", " + coords.y);

		float x = Mathf.Round(coords.x);
		float y = Mathf.Round(coords.y);

		//Debug.Log(this.name + ": Rounded: " + x + ", " + y);

		x += (mapWidth / 2.0f);
		y += (mapHeight / 2.0f);

		//Debug.Log(this.name + ": Corrected: " + x + ", " + y);

		coords.x = x;
		coords.y = y;

		//Debug.Log(this.name + ": Initial: " + coords.x + ", " + coords.y);

		return coords;
	}

	public Tile GetTile(Vector2 _Coords)
	{
		if(_Coords.x < 0.0f || _Coords.x >= (float)mapWidth || _Coords.y < 0.0f || _Coords.y >= (float)mapHeight)
			return null;

		return tiles[(int)_Coords.x, (int)_Coords.y];
	}

	public Sprite GetNextTileSprite(Tile.EType _Type)
	{
		Sprite next = null;

		switch(_Type)
		{ 
			case Tile.EType.Factory:
				next = FactoryTile.Renderer.sprite;
				break;

			case Tile.EType.Production:
				next = ProductionTiles[nextProduction].Renderer.sprite;
				break;

			case Tile.EType.Housing:
				next = HousingTiles[nextHousing].Renderer.sprite;
				break;
		}

		return next;
	}

	public void WitherAll()
	{
		//Debug.Log(this.name + ": Wither all.");
		StartCoroutine(SlowlyWitherAll(0.01f, 0.03f));
	}

	private IEnumerator SlowlyWitherAll(float _Min, float _Max)
	{
		Camera.main.backgroundColor = Color.black;

		for(int y = 0; y < mapHeight; y++)
		{
			for(int x = 0; x < mapWidth; x++)
			{
				if(tiles[x, y] != null && tiles[x, y].Type != Tile.EType.Broken)
				{
					//Debug.Log(this.name + ": Withering " + tiles[x, y].name);

					tiles[x, y].Wither(BrokenTile);
					tiles[x, y].transform.SetParent(null);
					yield return new WaitForSeconds(Random.Range(_Min, _Max));
				}
			}
		}

		Destroy(gameObject);
	}

	public float GetWitherProgress()
	{
		return (float)amountBroken / (float)(mapWidth * mapHeight);
	}
}
