﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopupText : MonoBehaviour 
{
	public float VanishSpeed = 1.0f;
	public float FloatSpeed = 1.0f;

	private RectTransform rectTrans;
	private Text text;
	private Color color;

	void Awake()
	{
		rectTrans = GetComponent<RectTransform>();
		text = GetComponent<Text>();
		if(text == null)
		{
			Destroy(gameObject, 0.1f);
			return;
		}

		color = text.color;
	}

	void Update()
	{
		rectTrans.anchoredPosition += Vector2.up * FloatSpeed * Time.deltaTime;

		color.a -= VanishSpeed * Time.deltaTime;
		text.color = color;

		if(text.color.a <= 0.0f)
			Destroy(gameObject);
	}
}
