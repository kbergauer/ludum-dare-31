﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Production : Building 
{
	[Header("Production parameters")]
	public int Currency = 100;
	public float Intervall = 1.0f;
	public Text CurrencyPopup = null;

	private GameManager manager = null;
	private RectTransform canvas = null;
	private float timer = 0.0f;

	protected override void Start()
	{
		GameObject go = GameObject.FindGameObjectWithTag("GameManager");
		if(go != null)
			manager = go.GetComponent<GameManager>();

		go = GameObject.FindGameObjectWithTag("PopupContainer");
		if(go != null)
			canvas = go.GetComponent<RectTransform>();

		base.Start();
	}

	void Update()
	{
		timer += Time.deltaTime;
		if(timer >= Intervall)
		{
			timer = 0.0f;
			Produce();
		}
	}

	private void Produce()
	{
		if(manager == null)
			return;

		// Add amount to funds
		manager.Currency += Currency;

		// Display addition in UI
		Text popup = Instantiate(CurrencyPopup) as Text;
		popup.text = "+" + Currency;
		popup.rectTransform.SetParent(canvas);
		popup.rectTransform.position = Camera.main.WorldToScreenPoint(transform.position);
	}
}