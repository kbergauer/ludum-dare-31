﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectionButton : MonoBehaviour 
{
	public Tile.EType Selection = Tile.EType.Factory;

	public Button[] Rivals = null;

	private GameManager manager;
	private Button button;

	void Awake()
	{
		GameObject go = GameObject.FindGameObjectWithTag("GameManager");
		if(go != null)
			manager = go.GetComponent<GameManager>();

		button = GetComponent<Button>();
	}

	void OnEnable()
	{
		// Set correct selection in menu popup
		if(button != null)
		{
			if(manager != null && manager.GetSelection() != Selection)
				button.interactable = true;
			else
				button.interactable = false;
		}
	}

	public void Select()
	{
		// Set selection
		if(manager != null)
		{
			// Check if selection change was successful
			bool changed = manager.SetSelection(Selection);
			if(changed)
			{
				// Disable button and copy
				if(button != null)
					button.interactable = false;

				// Enable the rivaling buttons
				for(int i = 0; i < Rivals.Length; i++)
					Rivals[i].interactable = true;
			}
		}
	}

	public void SetInteraction(bool _Interactable)
	{
		//Debug.Log(this.name + ": Setting my interaction to " + _Interactable);

		// Set own interaction
		if(button != null)
			button.interactable = _Interactable;

		// Set rival button interaction
		for(int i = 0; i < Rivals.Length; i++)
			Rivals[i].interactable = !_Interactable;
	}
}