﻿using UnityEngine;
using System.Collections;

[SelectionBase]
public class Tile : MonoBehaviour
{
	public delegate void TileHandler(Tile _Target);
	public event TileHandler OnWither;

	public enum EType
	{ 
		Empty,
		Broken,
		Factory,
		Production,
		Occupied,
		Housing
	}

	[Header("Map parameters")]
	public EType Type = EType.Empty;
	public SpriteRenderer Renderer;

	[Header("Wither parameters")]
	public float Acceleration = 1.0f;
	public float RotationSpeed = 1.0f;
	public AudioClip WitherSound = null;

	// Withering
	private float rotDirection = 1.0f;
	private float speed = 0.0f;
	private float angle = 0.0f;

	private WorkManager work = null;
	private Vector2 position = Vector2.zero;

	void Awake()
	{
		Renderer = GetComponent<SpriteRenderer>();

		GameObject go = GameObject.FindGameObjectWithTag("WorkManager");
		if(go != null)
			work = go.GetComponent<WorkManager>();
	}

	public void SetPosition(int _X, int _Y)
	{
		position = new Vector2(_X, _Y);
	}

	public bool Wither(Tile _Broken)
	{
		// Do not wither broken tiles
		if(Type == EType.Broken)
			return false;

		// Fire event
		if(OnWither != null)
			OnWither(this);

		// Generate repair job
		if(work != null && _Broken != null)
			work.AddJob((int)position.x, (int)position.y, _Broken);

		// Change color and sorting order
		if(Renderer != null)
		{
			Renderer.color = Color.gray * Renderer.color;
			Renderer.sortingOrder = 10000;
		}

		// Play sound
		if(GameManager.PlaySound && WitherSound != null)
			AudioSource.PlayClipAtPoint(WitherSound, transform.position);

		// Set broken
		Type = Tile.EType.Broken;

		// Rotate
		if(Random.Range(0.0f, 1.0f) < 0.5f)
			rotDirection = -1.0f;

		// Start falling
		StartCoroutine(Fall());

		return true;
	}

	private IEnumerator Fall()
	{
		while(Renderer.isVisible)
		{
			// Move
			speed += Acceleration;
			transform.position += Vector3.down * speed * Time.deltaTime;

			// Rotate
			angle += RotationSpeed * rotDirection;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

			// Wait for next turn
			yield return new WaitForEndOfFrame();
		}

		Destroy(gameObject);
	}
}