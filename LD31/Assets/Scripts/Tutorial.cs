﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tutorial : MonoBehaviour
{
	public static bool TutorialShown = false;

	public Text TextField = null;
	public Text PageNumber = null;
	public Text NextButton = null;
	public TutorialElement[] Messages = null;

	private int currentMessage = 0;

	void Awake()
	{
		Time.timeScale = 0.0f;

		if(!TutorialShown)
			Show(0);
		else
			Close();
	}

	public void Next()
	{
		if(!Show(currentMessage + 1))
			Close();
	}

	public void Previous()
	{
		Show(currentMessage - 1);
	}

	public bool Show(int _ID)
	{
		// Abort with invalid id
		if(_ID < 0 || _ID >= Messages.Length || TextField == null)
			return false;

		// Hide old indicator
		if(Messages[currentMessage].Indicator != null)
			Messages[currentMessage].Indicator.SetActive(false);

		// Show next message and indicator
		TextField.text = Messages[_ID].Message;
		if(Messages[_ID].Indicator != null)
			Messages[_ID].Indicator.SetActive(true);

		// Update page number
		if(PageNumber != null)
			PageNumber.text = (_ID + 1).ToString("00") + "/" + Messages.Length.ToString("00");

		// Update next button content
		if(NextButton != null)
		{
			if(_ID == Messages.Length - 1)
				NextButton.text = "Close";
			else
				NextButton.text = "Next";
		}
		
		// Set new message id
		currentMessage = _ID;

		return true;
	}

	public void Close()
	{
		TutorialShown = true;

		Time.timeScale = 1.0f;
		Destroy(gameObject);
	}
}