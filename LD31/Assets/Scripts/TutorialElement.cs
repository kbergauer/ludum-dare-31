﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TutorialElement 
{
	public string Message = "";
	public GameObject Indicator = null;
}