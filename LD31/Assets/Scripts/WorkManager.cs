﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorkManager : MonoBehaviour 
{
	public AudioClip TilePlacementSound = null;

	private Map map;
	private List<Job> jobs;
	private List<Factory> factories;

	void Awake()
	{
		GameObject go = GameObject.FindGameObjectWithTag("Map");
		if(go != null)
			map = go.GetComponent<Map>();

		jobs = new List<Job>();
		factories = new List<Factory>();
	}

	void Start()
	{ 
		// Get initial housing list
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Factory");
		Factory factory = null;
		for(int i = 0; i < objects.Length; i++)
		{
			factory = objects[i].GetComponent<Factory>();
			if(factory != null)
			{
				//Debug.Log(this.name + ": Found housing " + housing.name);
				factories.Add(factory);
				factory.OnWither += RemoveFactory;
			}
		}
	}

	// Add a job to the list of open jobs
	public void AddJob(int _X, int _Y, Tile _Broken)
	{
		jobs.Add(new Job(_X, _Y, _Broken));
	}

	// Return the closest open job to the position
	public Job GetJob(Vector2 _Position)
	{
		if(jobs.Count == 0)
			return null;

		Job result = null;
		float distance = float.PositiveInfinity;
		float closest = float.PositiveInfinity;

		for(int i = 0; i < jobs.Count; i++)
		{
			distance = Vector2.Distance(jobs[i].Broken.transform.position, _Position);
			if(distance < closest)
			{
				result = jobs[i];
				closest = distance;
			}
		}

		jobs.Remove(result);
		return result;
	}

	// Try finishing a job at the specified position
	public bool FinishJob(Vector3 _Position, Job _Job)
	{
		if(_Position != _Job.Broken.transform.position)
			return false;

		bool result = _Job.Finish(map);

		if(result && GameManager.PlaySound && TilePlacementSound != null)
			AudioSource.PlayClipAtPoint(TilePlacementSound, _Job.Replacement.transform.position);

		return result;
	}

	// Return the closest housing
	public Factory GetClosestFactory(Vector3 _Position)
	{
		Factory result = null;
		float distance = float.PositiveInfinity;
		float closest = float.PositiveInfinity;
		for(int i = 0; i < factories.Count; i++)
		{
			if(factories[i] == null)
				continue;

			distance = Vector3.Distance(factories[i].transform.position, _Position);
			//Debug.Log(this.name + ": Distance to " + factories[i].name + ": " + distance);
			if(distance < closest)
			{
				closest = distance;
				result = factories[i];
			}
		}

		return result;
	}

	// Create new tile
	public void GetTile(Job _Job)
	{
		_Job.Replacement = map.CreateTile((int)_Job.Position.x, (int)_Job.Position.y, Tile.EType.Empty);
	}

	public void AddFactory(Factory _Target)
	{
		if(!factories.Contains(_Target))
			factories.Add(_Target);
	}

	private void RemoveFactory(Tile _Target)
	{
		Factory factory = _Target as Factory;

		if(factories.Contains(factory))
		{
			factories.Remove(factory);
			factory.OnWither -= RemoveFactory;
		}
	}
}
