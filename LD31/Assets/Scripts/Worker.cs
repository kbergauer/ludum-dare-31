﻿using UnityEngine;
using System.Collections;

[SelectionBase]
public class Worker : MonoBehaviour 
{
	[Header("Life")]
	public Vector2 Lifespan = new Vector2(5.0f, 15.0f);

	[Header("Job")]
	public float Speed = 1.0f;
	public Transform CarryPoint = null;
	public Vector3 CarryOffset = Vector3.zero;
	public GameObject ConstructionSprite = null;

	[Header("Wandering")]
	public Vector2 Intervall = Vector2.one;

	[Header("Sound")]
	public AudioClip SpawnSound;
	public AudioClip DeathSound;

	// General
	private Animator animator = null;
	private SpriteRenderer rend = null;
	private Vector3 position;
	private Vector3 scale;

	// Life
	private Factory manufacturer;
	private float life = 0.0f;
	private float lifeTimer = 0.0f;
	private bool isDead = false;

	// Work
	private WorkManager manager = null;
	private Job job = null;
	private Factory factory = null;
	private Vector3 jobDirection = Vector3.zero;

	// Wander
	private float wanderTimer = 0.0f;
	private float wanderIntervall = 0.0f;
	private bool isWandering = false;
	private Vector3 wanderDirection = Vector3.zero;


	void Awake()
	{
		animator = GetComponent<Animator>();
		rend = transform.FindChild("Sprite").GetComponent<SpriteRenderer>();
		scale = transform.localScale;

		life = Random.Range(Lifespan.x, Lifespan.y);

		wanderIntervall = Random.Range(Intervall.x, Intervall.y);

		GameObject go = GameObject.FindGameObjectWithTag("WorkManager");
		if(go != null)
			manager = go.GetComponent<WorkManager>();

		if(manager == null)
			Destroy(gameObject);
		else
			transform.parent = manager.transform;
	}

	void Start()
	{
		if(GameManager.PlaySound && SpawnSound != null)
			AudioSource.PlayClipAtPoint(SpawnSound, transform.position);
	}

	void Update()
	{
		if(isDead || CheckDeath())
			return;

		// Set sorting order
		SetSortingOrder();

		// Get a job
		if(job == null)
		{
			factory = manager.GetClosestFactory(transform.position);
			job = manager.GetJob(factory == null ? transform.position : factory.transform.position);

			if(job != null)
				GetDirectionToFactory();
		}

		// Work job
		if(job != null)
			Work();
		// Wander aimlessly
		else
			Wander();

		ClampPosition();
	}

	private void SetSortingOrder()
	{
		rend.sortingOrder = -Mathf.RoundToInt(transform.position.y * 100.0f);
		if(job != null && job.Replacement != null)
		{
			job.Replacement.Renderer.sortingOrder = rend.sortingOrder;
			ConstructionSprite.renderer.sortingOrder = rend.sortingOrder + 1;
		}
	}

	private void ClampPosition()
	{
		position = transform.position;
		position.x = Mathf.Clamp(position.x, -16.5f, 15.5f);
		position.y = Mathf.Clamp(position.y, -10.5f, 9.5f);

		transform.position = position;
	}

	private void Work()
	{
		// Try finishing job
		bool finished = manager.FinishJob(transform.position, job);

		if(finished)
		{
			animator.SetBool("IsWalking", false);
			animator.SetBool("IsCarrying", false);

			ConstructionSprite.SetActive(false);

			job = null;
		}
		// Act according to job
		else
		{
			// Turn in walk direction
			if(jobDirection.x >= 0.0f)
			{
				scale.x = Mathf.Abs(scale.x);
				transform.localScale = scale;
			}
			else
			{
				scale.x = -Mathf.Abs(scale.x);
				transform.localScale = scale;
			}

			// Get replacement
			if(job.Replacement == null)
			{
				// Check housing validity
				if(factory != null && factory.Type == Tile.EType.Broken)
					factory = null;

				// Get walking direction
				GetDirectionToFactory();

				// Move to housing tile
				if(factory != null)
				{
					if(!animator.GetBool("IsWalking"))
						animator.SetBool("IsWalking", true);

					if(Vector3.Distance(factory.transform.position, transform.position) > 0.1f)
						transform.Translate(jobDirection * Speed * Time.deltaTime);
					else
					{
						// Grab tile
						manager.GetTile(job);
						if(job.Replacement != null)
						{
							// Set replacement position and parent
							job.Replacement.transform.position = CarryPoint.position + CarryOffset;
							job.Replacement.transform.parent = CarryPoint;
							job.Replacement.Renderer.sortingOrder = 7000;

							ConstructionSprite.SetActive(true);

							// Get direction to repair site
							jobDirection = (job.Broken.transform.position - transform.position).normalized;
						}
					}
				}
				// Wait for housing replacement
				else
				{
					if(animator.GetBool("IsWalking"))
						animator.SetBool("IsWalking", false);
				}
			}
			// Move to repair tile
			else
			{
				// Get direction
				GetDirectionToJob();

				if(transform.position != job.Broken.transform.position)
				{
					if(Vector3.Distance(job.Broken.transform.position, transform.position) > 0.1f)
						transform.Translate(jobDirection * Speed * Time.deltaTime);
					else
						transform.position = job.Broken.transform.position;

					if(!animator.GetBool("IsCarrying"))
						animator.SetBool("IsCarrying", true);
				}
			}
		}
	}

	private void GetDirectionToFactory()
	{
		// Try to get factory if none is assigned
		if(factory == null)
			factory = manager.GetClosestFactory(transform.position);

		// Get direction to factory
		if(factory != null)
		{
			jobDirection = (factory.transform.position - transform.position).normalized;
			animator.SetBool("IsWalking", true);
		}
	}

	private void GetDirectionToJob()
	{
		// Get direction to job
		jobDirection = (job.Broken.transform.position - transform.position).normalized;
	}

	private void Wander()
	{
		// Turn in walk direction
		if(wanderDirection.x >= 0.0f)
		{
			scale.x = Mathf.Abs(scale.x);
			transform.localScale = scale;
		}
		else
		{
			scale.x = -Mathf.Abs(scale.x);
			transform.localScale = scale;
		}

		// Choose behaviour
		wanderTimer += Time.deltaTime;
		if(wanderTimer >= wanderIntervall)
		{
			wanderTimer = 0.0f;
			wanderIntervall = Random.Range(Intervall.x, Intervall.y);

			isWandering = !isWandering;
			if(isWandering)
			{
				wanderDirection = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward) * Vector3.up;
				animator.SetBool("IsWalking", true);
			}
			else
				animator.SetBool("IsWalking", false);
		}

		// Execute behaviour
		if(isWandering)
		{
			if(!animator.GetBool("IsWalking"))
				animator.SetBool("IsWalking", true);

			// Walk
			transform.Translate(wanderDirection * (Speed * 0.25f) * Time.deltaTime);
		}
	}

	private bool CheckDeath()
	{
		lifeTimer += Time.deltaTime;
		if(lifeTimer >= life)
		{
			if(job != null)
			{
				// Wither tile replacement
				if(job.Replacement != null)
				{
					job.Replacement.transform.SetParent(null);
					ConstructionSprite.transform.SetParent(job.Replacement.transform);
					ConstructionSprite.renderer.sortingOrder = 11000;
					job.Replacement.Wither(null);
				}

				// Readd job to queue
				manager.AddJob((int)job.Position.x, (int)job.Position.y, job.Broken);
				job = null;
			}

			// Show animation
			animator.SetTrigger("Death");
			isDead = true;

			// Play sound
			if(GameManager.PlaySound && DeathSound != null)
				AudioSource.PlayClipAtPoint(DeathSound, transform.position);

			return true;
		}

		return false;
	}

	private void Destroy()
	{
		manufacturer.RemoveWorker(this);
		Destroy(gameObject);
	}

	public void SetManufacturer(Factory _Factory)
	{
		manufacturer = _Factory;
	}
}
