﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WorkerSpawn 
{
	public Worker Prefab;
	public float Chance;
}